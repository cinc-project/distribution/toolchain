#!/bin/bash -e
#
# Author:: Lance Albertson <lance@osuosl.org>
# Copyright:: Copyright 2024, Cinc Project
# License:: Apache License, Version 2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

TOP_DIR="$(pwd)"
export CI_PROJECT_DIR=${CI_PROJECT_DIR:-${TOP_DIR}}
# Needed to build gtar properly
export FORCE_UNSAFE_CONFIGURE=1
export rvm_path="${TOP_DIR}/rvm"
set -ex
mkdir -p ${TOP_DIR}/cache/git_cache
echo "cache_dir '${TOP_DIR}/cache'" >> omnibus-toolchain/omnibus.rb
echo "git_cache_dir '${TOP_DIR}/cache/git_cache'" >> omnibus-toolchain/omnibus.rb
echo "use_git_caching true" >> omnibus-toolchain/omnibus.rb

if command -v dnf &>/dev/null; then
  dnf install -y autoconf bison flex gcc gcc-c++ gettext kernel-devel make m4 \
    ncurses-devel patch git rpm-build which procps-ng perl-Digest-SHA perl-IPC-Cmd \
    perl-bignum perl-FindBin perl-lib automake
elif command -v apt &>/dev/null; then
  apt update
  apt -y install autoconf bison build-essential flex gettext libncurses-dev \
    libssl-dev libreadline-dev zlib1g-dev git libffi8 libffi-dev debianutils \
    procps dpkg-dev devscripts automake
fi

set +x
curl -sSL https://get.rvm.io | bash
source "${TOP_DIR}/rvm/scripts/rvm"
rvm install ruby-3.1
cd omnibus-toolchain
gem install -N bundler:2.3.26
set -x
bundle lock
git config --global user.email || git config --global user.email "maintainers@cinc.sh"
git add Gemfile.lock
git commit -m 'Update Gemfile.lock to cinc sources'
bundle install --without development --path "${TOP_DIR}/vendor/bundle"
bundle exec omnibus build omnibus-toolchain -l ${OMNIBUS_LOG_LEVEL:-info} --override append_timestamp:false
